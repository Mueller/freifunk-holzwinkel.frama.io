---
title: "Kontakt"
date: 2018-10-21T17:25:00+06:00
author:
tags: []
---
# Kontakt

Der allgemeine Austausch zu Freifunk Holzwinkel findet im freien und dezentralisierten Instant-Messaging-Netzwerk [Matrix](https://matrix.org) unter folgender Adresse statt: [#freifunk-holzwinkel:tchncs.de](https://matrix.to/#/#freifunk-holzwinkel:tchncs.de)

<script>
function showEmailAddress(){
if(confirm("Wollen Sie die E-Mail-Adresse wirklich sehen?")){
alert(shiftString("\ufff7\u0003\ufff6\ufffa\ufff7\u0006\uffff\ufffcﾾ\ufff9\u0000�\u000b\u0008\ufffa\uffff\ufffc\ufff6�\u000c￬Uￖ￥￥￥￮\u000e\u0004\u0000�\ufffa\ufff5\ufff2\u0003\ufffa\u0004﾿\ufffe\ufff6", 111));
}
}

function shiftString(text, distance){
    var shiftedText="";
    for(var i=0; i<text.length; i++){
        shiftedText+=String.fromCharCode(text.charCodeAt(i)+distance);
    }
    return shiftedText;
}
</script>

Für privatere Anfragen kann die E-Mail-Adresse genutzt werden: [E-Mail-Adresse anzeigen](javascript: showEmailAddress())
