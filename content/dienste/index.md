---
title: "Dienste"
date: 2018-08-31T20:35:47+06:00
author:
tags: []
---
# Lokal bereitgestellte Dienste im Freifunk Holzwinkel-Netz

Folgende Dienste können im Freifunk Holzwinkel-Netz genutzt werden, sind aber nicht aus dem Internet erreichbar:

* [Radio Holzwinkel](ipfs://radio.ffhw) (noch nicht in Betrieb)
* [Wetterstation](ipfs://wetter.ffhw) (noch nicht in Betrieb)
* [IPFS](ipfs://ipfs.ffhw) (noch nicht in Betrieb)
