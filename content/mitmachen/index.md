---
title: "Mitmachen"
date: 2018-08-31T20:35:47+06:00
author:
tags: []
---
# Wie kann ich mich an Freifunk Holzwinkel beteiligen?

* Router aufstellen
* anderen Leuten von Freifunk Holzwinkel erzählen
* Firmware und andere Softwares weiterentwickeln und verbessern
* zu den regelmäßigen Freifunk-Treffen kommen
* Texte für die Webseite, Flyer und ähnliches schreiben
* lokale Dienste betreiben
* Infomaterial wie Flyer, Plakate und Aufkleber erstellen und verbessern
* [Antennen bauen und Router outdoorfähig machen](../anleitungen)